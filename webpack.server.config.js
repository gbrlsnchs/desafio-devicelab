const path = require('path');
const nodeExternals = require('webpack-node-externals');
const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
  context: path.resolve(__dirname, 'src/server'),
  entry: [
    'babel-polyfill',
    './index.js'
  ],
  output: {
    filename: 'server.bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: isDev ? 'eval' : 'nosources-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src/server'),
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'server': path.resolve(__dirname, 'src/server')
    }
  },
  target: 'node',
  externals: [nodeExternals()],
  watch: isDev
}
