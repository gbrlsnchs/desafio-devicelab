import redis from 'redis';

import { CACHE_URI, CACHE_PORT, CACHE_PASS, CACHE_NAME } from 'server/config/constants';

const redisClient = redis.createClient(CACHE_PORT, CACHE_URI, { password: CACHE_PASS });

export default redisClient;
