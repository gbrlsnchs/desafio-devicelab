import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import path from 'path';

import connection from 'server/config/db';
import apiRouter from 'server/api';
import jwtMiddleware from 'server/middlewares/jwt';

const app = express();
app.disable('x-powered-by');
app.use(express.static('dist/public'));
app.get('/', (req, res) => {
  const index = path.resolve(__dirname, 'public/index.html');
  console.log('index path =', index);
  res.sendFile(index);
});

app.use(bodyParser.json({ type: '*/*' }));
app.use('/api', jwtMiddleware, apiRouter);
app.use('/api', (err, req, res, next) => {
  if (!err) {
    next();

    return;
  }

  const invalidToken = err.name === 'UnauthorizedError';
  const statusCode = invalidToken ? 401 : 400;
  const message = invalidToken ? 'Token inválido' : 'Bad request';

  res.status(statusCode).json({ message });
});
app.use('*', (req, res) => res.status(401).json({ message: 'Não autorizado' }));

const httpServer = http.createServer(app);
export default httpServer;
