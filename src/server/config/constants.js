import settings from 'server/settings.json';

process.env = { ...process.env, ...settings };

export const {
  DB_USER,
  DB_PASS,
  DB_URI,
  DB_PORT,
  DB_NAME,
  CACHE_URI,
  CACHE_PORT,
  CACHE_PASS,
  CACHE_NAME,
  JWT_SECRET
} = process.env;
