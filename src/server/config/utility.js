import jwt from 'jsonwebtoken';
import uuidv4 from 'uuid/v4';

import { JWT_SECRET } from 'server/config/constants';

export const getToken = (id) => {
  const options = {
    expiresIn: '5m'
  };

  const payload = {
    sub: id,
    jti: uuidv4()
  };

  const token = jwt.sign(payload, new Buffer(JWT_SECRET, 'base64'), options);

  return { token, payload };
};
