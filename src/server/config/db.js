import mongoose from 'mongoose';

import { DB_USER, DB_PASS, DB_URI, DB_PORT, DB_NAME } from 'server/config/constants';

mongoose.Promise = global.Promise; 
const connection = mongoose.createConnection(`mongodb://${DB_USER}:${DB_PASS}@${DB_URI}:${DB_PORT}/${DB_NAME}`);

export default connection;
