import connection from 'server/config/db';

import redisClient from 'server/config/cache';

const postHandler = (req, res) => {
  const { jti, sub } = req.user;

  redisClient.set(jti, sub, (err, reply) => {
    if (err) {
      res.status(400).json({ message: 'Ocorreu um erro' });

      return;
    }

    if (reply !== 'OK') {
      res.status(401).json({ message: 'Ocorreu um erro com o Redis' });

      return;
    }

    const expireTime = req.user.exp - req.user.iat;

    redisClient.expire(jti, expireTime, (err, reply) => {
      if (err) {
        redisClient.del(jti);
        res.status(400).json({ message: err });

        return;
      }

      if (reply !== 1) {
        redisClient.del(jti);
        res.status(401).json({ message: reply });

        return;
      }

      res.status(200).json({ ok: true });
    });
  });
};

export default postHandler;
