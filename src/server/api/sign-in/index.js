import { Router } from 'express';

import postHandler from 'server/api/sign-in/post';

const signInRouter = Router();
signInRouter.post('/', postHandler);

export default signInRouter;
