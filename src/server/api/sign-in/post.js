import bcrypt from 'bcryptjs';

import UserModel from 'server/models/user';
import * as utility from 'server/config/utility';

const postHandler = async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    res.status(400).json({ message: 'Campos inválidos e/ou faltando' });

    if (!email) {
      console.log('Falta o campo "e-mail"');
    }

    if (!password) {
      console.log('Falta o campo "senha"');
    }

    return;
  }

  try {
    let user = await UserModel.findOne({ email }, '_id password').exec();

    if (!user) {
      res.status(400).json({ message: 'Usuário e/ou senha estão incorretos' });
      console.log('Usuário inexistente');

      return;
    }

    const valid = bcrypt.compareSync(password, user.password);

    if (!valid) {
      res.status(403).json({ message: 'Usuário e/ou senha estão incorretos' });
      console.log('Senha inválida');

      return;
    }

    user.last_access = Date.now();
    user = await user.save();

    const { token, payload } = utility.getToken(user._id);

    res.status(200).json({ token });
  } catch (e) {
    res.status(400).json({ message: 'Ocorreu um erro de requisição' });
  }
};

export default postHandler;
