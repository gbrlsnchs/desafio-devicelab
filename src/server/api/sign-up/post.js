import bcrypt from 'bcryptjs';

import UserModel from 'server/models/user';
import * as utility from 'server/config/utility';

const postHandler = async (req, res) => {
  const { name, email } = req.body;
  let { password } = req.body;

  if (!name || !email || !password) {
    res.status(400).json({ message: 'Campos inválidos e/ou faltando' });

    return;
  }

  // 18 caracteres é o limite aproximado (seguro) da biblioteca bcryptjs
  // segundo a documentação: https://github.com/dcodeIO/bcrypt.js
  if (password.length < 6 || password.length > 18) {
    res.status(400).json({ message: 'A senha deve conter entre 6 a 18 caracteres' });

    return;
  }

  try {
    let user = await UserModel.findOne({ email }).exec();

    if (user) {
      res.status(400).json({ message: 'Usuário já existente' });

      return;
    }

    const salt = bcrypt.genSaltSync();
    password = bcrypt.hashSync(password, salt);

    user = new UserModel({
      name,
      email,
      password
    });

    user = await user.save();

    const { token } = utility.getToken(user._id);

    res.status(200).json({ token });
  } catch (e) {
    res.status(400).json({ message: e });
  }
};

export default postHandler;
