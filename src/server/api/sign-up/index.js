import { Router } from 'express';

import postHandler from 'server/api/sign-up/post';

const signOutRouter = Router();
signOutRouter.post('/', postHandler);

export default signOutRouter;
