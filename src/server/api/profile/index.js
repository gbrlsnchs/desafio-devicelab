import { Router } from 'express';
import getHandler from 'server/api/profile/get';
import connection from 'server/config/db';

const profileRouter = Router();
profileRouter.get('/', getHandler);

export default profileRouter;
