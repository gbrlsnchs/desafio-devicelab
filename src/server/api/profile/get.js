import connection from 'server/config/db';
import UserModel from 'server/models/user';

const getHandler = async (req, res) => {
  const { sub } = req.user;
  const fields = [
    'name',
    'email',
    'creation_date',
    'last_access',
    '-_id'
  ].join(' ');

  try {
    const user = await UserModel.findOne({ _id: sub }, fields);

    res.status(200).json(user);
  } catch (e) {
    res.status(400).json({ message: 'Ocorreu um erro de requisição' });
  }

  res.json({ data: 'ok' });
};

export default getHandler;
