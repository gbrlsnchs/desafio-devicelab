import { Router } from 'express';

import profileRouter from 'server/api/profile';
import signInRouter from 'server/api/sign-in';
import signOutRouter from 'server/api/sign-out';
import signUpRouter from 'server/api/sign-up';
const apiRouter = Router();

apiRouter.use('/profile', profileRouter);
apiRouter.use('/sign-in', signInRouter);
apiRouter.use('/sign-out', signOutRouter);
apiRouter.use('/sign-up', signUpRouter);

export default apiRouter;
