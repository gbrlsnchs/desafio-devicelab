import { Schema } from 'mongoose';

import connection from 'server/config/db';

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  creation_date: {
    type: Date,
    default: Date.now()
  },
  last_access: {
    type: Date,
    default: Date.now()
  }
});

const UserModel = connection.model('User', userSchema);
export default UserModel;
