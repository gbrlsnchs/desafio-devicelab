import httpServer from 'server/config/http-server';

httpServer.on('listening', () => {
  console.log('listening...');
})

const port = process.env.PORT || 3000;
httpServer.listen(port);
