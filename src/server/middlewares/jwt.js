import jwt from 'express-jwt';

import redisClient from 'server/config/cache';
import { JWT_SECRET } from 'server/config/constants';

const jwtOptions = {
  secret: new Buffer(JWT_SECRET, 'base64'),
  getToken: (req) => req.headers['auth-token'],
  isRevoked: (req, payload, done) => {
    const { jti, sub } = payload;

    redisClient.get(jti, (err, reply) => {
      if (err) {
        return done(err);
      }

      return done(null, reply === sub);
    });
  }
};
const authorizedPaths = [
  'sign-in',
  'sign-up'
];

const jwtMiddleware = jwt(jwtOptions).unless({ path: authorizedPaths.map(path => `/api/${path}`) });

export default jwtMiddleware;
