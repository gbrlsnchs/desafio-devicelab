export default class HomeController {
  constructor($http, $timeout, $location, $window) {
    this.$http = $http;
    this.$timeout = $timeout;
    this.$location = $location;
    this.$window = $window;
    this.cleanForm();
    this.selectedTab = 'login';
    this.message = '';
  }

  switchContext(context) {
    this.selectedTab = context;
    this.cleanForm();
  }

  cleanForm() {
    this.form = {
      name: '',
      email: '',
      password: ''
    };
  }

  async submit() {
    this.message = '';

    let endpoint = this.selectedTab === 'login' ? 'sign-in' : 'sign-up';
    endpoint = `/api/${endpoint}`;

    try {
      const response = await this.$http.post(endpoint, this.form);
      const { data } = response;

      this.$window.localStorage.setItem('auth-token', data.token);
      this.$timeout(_ => this.$location.path('/profile'));
    } catch (response) {
      this.$timeout(_ => { this.message = response.data.message });
    }
  }
}
