export default class ProfileController {
  constructor($http, $location, $window, $timeout) {
    this.$http = $http;
    this.$location = $location;
    this.$window = $window;
    this.$timeout = $timeout;
    this.token = $window.localStorage.getItem('auth-token');
  }

  async $onInit() {
    try {
      const response = await this.$http.get('/api/profile', {
        headers: {
          'AUTH-TOKEN': this.token
        }
      });

      this.$timeout(_ => this.form = response.data);
    } catch (response) {
      this.$window.alert(response.data.message);
      this.$timeout(_ => this.$location.path('/'));
    }
  }

  async submit() {
    try {
      const response = await this.$http.post('/api/sign-out', null, {
        headers: {
          'AUTH-TOKEN': this.token
        }
      });
    } catch (response) {
      this.$window.alert(response.data.message);
    } finally {
      this.$timeout(_ => this.$location.path('/'));
    }
  }
}
