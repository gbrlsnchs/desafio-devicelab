import angular from 'angular';
import angularRoute from 'angular-route';

import HomeController from 'client/pages/home/home.controller';
import homeTemplate from 'client/pages/home/home.html';

import ProfileController from 'client/pages/profile/profile.controller';
import profileTemplate from 'client/pages/profile/profile.html';

const app = angular.module('devicelab', [angularRoute]);

app.config($routeProvider => {
  $routeProvider
    .when('/', {
      template: homeTemplate
    })
    .when('/profile', {
      template: profileTemplate
    });
});

app.controller('HomeController', HomeController);
app.controller('ProfileController', ProfileController);
