const path = require('path');
const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
  context: path.resolve(__dirname, 'src/client'),
  entry: [
    'babel-polyfill',
    './index.js'
  ],
  output: {
    filename: 'client.bundle.js',
    path: path.resolve(__dirname, 'dist/public')
  },
  devtool: isDev ? 'eval' : 'nosources-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'client': path.resolve(__dirname, 'src/client')
    }
  },
  watch: isDev
}
